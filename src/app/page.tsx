'use client'

import styles from "./page.module.css";
import Image from "next/image";
import { FaPhoneAlt } from "react-icons/fa";

export default function Home() {
  return (
    <div className={styles.container}>

      <div className={styles.left}>

        <Image src="/myImage.png" className={styles.image} width={230} height={230} alt="My Image" />

        <div className={styles.contrat}>
          <h1>LIÊN HỆ VỚI TÔI</h1>
          <p>Q. Bình Tân, Tp. Hồ Chí Minh</p>
          <p>nvanhieu285@gmail.com</p>
          <p>0399990061</p>
        </div>

        <div className={styles.Summarize}>
          <h1>TÓM TẮT KĨ NĂNG</h1>
          <p>Quản lý thời gian hiệu quả</p>
          <p>Kỹ năng giao tiếp </p>
          <p>Dễ dàng thích nghi với môi trường mới</p>
          <p>Kỹ năng làm việc theo nhóm</p>
          <p>Lập kế hoạch</p>
          <p>Giải quyết vấn đề</p>
          <p>Tư duy sáng tạo</p>
        </div>

        <div className={styles.Language}>
          <h1>NGÔN NGỮ GIAO TIẾP</h1>
          <p>Tiếng anh cơ bản</p>
        </div>
      </div>

      <div className={styles.right}>

        <h1>NGUYỄN VĂN HIẾU</h1>
        <div className={styles.line}></div>
        <h2>LẬP TRÌNH VIÊN</h2>

        <div className={styles.Goal}>
          <h2>MỤC TIÊU NGHỀ NGHIỆP</h2>
          <p>Mong muốn tìm được chỗ làm ổn định lâu dài</p>
          <p>Mong muốn tìm được chỗ làm có cơ hội thăng tiến tốt</p>
          <p>Mong muốn tìm được chỗ làm có mức lương tốt</p>
          <p>Mong muốn tìm được nơi có cơ hội cống hiến bản thân tốt</p>
        </div>

        <div className={styles.experience}>
          <h2>KINH NGHIỆM LÀM VIỆC</h2>

          <div className={styles.E1}>Thiết kế phần mềm quản lý sản phẩm</div>
          <p>... | T2/2024 đến nay</p>
          <span>
            <p>Sản Phẩm:</p>
            <a className={styles.Link} href="https://github.com/nvanhieuu/Quan_Ly_San_Pham.git">https://github.com/nvanhieuu/Quan_Ly_San_Pham.git</a>
          </span>

          <div className={styles.E2}>Thiết kế giao diện các trang web bán sản phẩm</div>
          <p>... | T2/2024 đến nay</p>

          <span>
            <p>Sản Phẩm:</p>
            <a className={styles.Link} href="https://nvanhieuu.github.io/Web_Ban_San_Pham/">https://nvanhieuu.github.io/Web_Ban_San_Pham/</a>
          </span>
          <p>Ngôn Ngữ: HTML, CSS, JavaScript</p>

          <span>
            <p>Sản Phẩm:</p>
            <a className={styles.Link} href="https://nvanhieuu.github.io/Shop_Thu_Cung/">https://nvanhieuu.github.io/Shop_Thu_Cung/</a>
          </span>
          <p>Ngôn Ngữ: HTML, CSS, JavaScript</p>
        </div>

        <div className={styles.Educational}>
          <h2>QUÁ TRÌNH HỌC VẤN</h2>
          <div>Cao Đẳng Công Thương Việt Nam</div>
          <p>Công Nghệ Thông Tin | T10/2022 - T9/2024</p>
          <span>
            <p>Được học những kiến thức cơ bản về Công Nghệ Thông Tin</p>
            <p>Hiện vẫn đang học tại trường</p>
          </span>
        </div>

      </div>
    </div>
  );
}
